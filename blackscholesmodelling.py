from math import sqrt, log, exp, erf 
import random
from numpy import arange 
import matplotlib.pyplot as plt 
import pandas as pd 
import os
S0 = 100.0  # S0 = Stock price 
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  #r = risk-free interest rate 
q = 0.02  # q = dividend yield 
vol = 0.2  # vol = volatility 
Nsteps = 100  # Number or steps in MC
K = 100
sigma = 0.3

# 1. The analytical way using the Black-Scholes formula
print("1. The analytical way using the Black-Scholes formula:")
def d1(S0, K, r, q, T, sigma):
    return ((log(S0/K) + (r - q +  (sigma**2)/2)*T)/(sigma * sqrt(T)))
def d2(S0, K, r, q, T, sigma):
    return ((log(S0/K) + (r - q -  (sigma**2)/2)*T)/(sigma * sqrt(T)))
def C0(S0, K, r, T):
    return (S0 * erf(d1(S0, K, r, q, T, sigma)) - K * exp(-r*T) * erf(d2(S0, K, r, q, T, sigma)))

print(d1(S0, K, r, q, T, sigma))
print(d2(S0, K, r, q, T, sigma))
print(C0(S0, K, r, T))

# 2. Using a geometric Brownian motion
print("2. Using a geometric Brownian motion:")
def dSt(r, q, St, d, t, sigma, Wt):



